#include "mqtt_handler.hh"

MqttHandler::MqttHandler(char* mqtt_server, uint16_t mqtt_port,
                         char* client_id, char* sub_topic,
                         void (*callback)(char*, byte*, unsigned int)):
mqtt_server(mqtt_server),
mqtt_port(mqtt_port),
client_id(client_id),
sub_topic(sub_topic)
{
  wifi_client = new WiFiClient;
  mqtt_client = new PubSubClient(*wifi_client);
  this->mqtt_client->setServer(this->mqtt_server, this->mqtt_port);
  this->mqtt_client->setCallback(callback);
}

void MqttHandler::publish(const char* topic, const char* msg,
                          uint16_t len)
{
  this->mqtt_client->publish(topic, msg, len);
}

void MqttHandler::update()
{
  if(!this->mqtt_client->connected()) {
	  this->reconnect();
	}
  this->mqtt_client->loop();
}

void MqttHandler::callback(char* topic, byte* payload,
                           uint16_t length)
{
	//should send it on serial may be it is better that 
	//callback get out from this class
}

void MqttHandler::reconnect()
{
  if(!this->mqtt_client->connected())
  {
    delay(10);
  if(retry++ == 100)
  {
    WiFi.disconnect(true);
    delay(3000);
    ESP.restart();
  }
  Serial.print("retry => ");
  Serial.println(retry);
#ifdef debug
  Serial.println("Attempting MQTT connection ...");
#endif
  if(this->mqtt_client->connect(this->client_id))
    {
#ifdef debug
      Serial.println("MQTT client connected");
#endif
	  this->mqtt_client->subscribe(this->sub_topic);
	}
	else
	{
#ifdef debug
	  Serial.print("Failed, rc=");
	  Serial.print(this->mqtt_client->state());
	  Serial.println(" try again in 1 seconds");
#endif
	  //it should wait for 1 second and i will be add this
	}
}
}