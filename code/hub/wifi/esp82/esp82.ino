#include "wifi_handler.hh"
#include "mqtt_handler.hh"
#include "serial_packet_handler.hh"

#define led1_blink_time 500
#define led1 4
#define led2 5
//mqtt 
char* mqtt_server = "172.24.67.111";
uint16_t mqtt_port = 1883;
char* client_id = "hub1";
char* sub_topic = "hubp";
//ssid
const char* wifi_ssid = "WiFi";
const char* wifi_password = "$harafi95";

void callback(char* topic, byte* payload, unsigned int length);

MqttHandler *mqtt;
SerialPacketHandler *serial_packet;
//led time variables
uint32_t led1_last_time_update;
void setup() {
  delay(3000);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  digitalWrite(led1, HIGH);
  digitalWrite(led2, HIGH);
  delay(200);
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);

  serial_packet = new SerialPacketHandler(115200);
  wifimanager::setup();
  mqtt_server = wifimanager::mqtt_server;
  Serial.print("mqtt_server => ");
  Serial.println(mqtt_server);
  mqtt = new MqttHandler(mqtt_server, mqtt_port, client_id,
                         sub_topic, &callback);
  for(auto i = 0; i < 10; i++)
  {
    digitalWrite(led1, HIGH);
    digitalWrite(led2, LOW);
    delay(40);
    digitalWrite(led2, HIGH);
    digitalWrite(led1, LOW);
    delay(40);
  }
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  
  // put your setup code here, to run once:
  led1_last_time_update = millis();
}

void loop() {
  
  // put your main code here, to run repeatedly:
  mqtt->update();
 // mqtt->publish("test", "salam", 5);
 if(millis() - led1_last_time_update > led1_blink_time)
 {
  led1_last_time_update = millis();
  digitalWrite(led2, !digitalRead(led2));
  }
}

void callback(char* topic, byte* payload, unsigned int length)
{
  /*
  for(auto i = 0; i < length; i++)
  {
    Serial.print(payload[i]);
    Serial.print(",");
  }
  Serial.println();
  */
  serial_packet->send(payload, length);
  digitalWrite(led1, HIGH);
  delay(1);
  digitalWrite(led1, LOW);
}