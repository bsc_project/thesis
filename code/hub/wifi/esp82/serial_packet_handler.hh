#ifndef __PACKET_HANDLER__
#define __PACKET_HANDLER__

#include <Arduino.h>

class SerialPacketHandler{
	public:
		SerialPacketHandler();
		SerialPacketHandler(unsigned long serial_baud_rate);
		void send(byte* payload, uint16_t size);
	private:

};
#endif