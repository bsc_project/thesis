#ifndef __MQTT_HANDLER__
#define __MQTT_HANDLER__

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define debug 


class MqttHandler{
public:
  MqttHandler(char* mqtt_server, uint16_t mqtt_port,
              char* client_id, char* sub_topic,
              void (*callback)(char*, byte*, unsigned int));
  void publish(const char* topic, const char* msg, uint16_t len);
  void update();
private:
  void callback(char* topic, byte* payload, uint16_t length);
  void reconnect();
  WiFiClient *wifi_client;
  PubSubClient *mqtt_client;
  char* mqtt_server;
  char* client_id;
  char* sub_topic;
  uint16_t mqtt_port;
  uint8_t retry = 0;	
};
#endif