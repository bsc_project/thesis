#include "serial_packet_handler.hh"

SerialPacketHandler::SerialPacketHandler(unsigned long serial_baud_rate)
{
  Serial.begin(serial_baud_rate);
}

SerialPacketHandler::SerialPacketHandler()
{
  SerialPacketHandler(115200);
}

void SerialPacketHandler::send(byte* payload, uint16_t size)
{
  uint8_t check_sum = 0;
  Serial.write(255);
  Serial.write(255);
  Serial.write(payload[0]);
  Serial.write(3);
  Serial.write(payload[1]);
  Serial.write(payload[2]);
  for(auto i = 0; i  < size - 1; i++)
    check_sum += payload[i];
  check_sum += size - 1;
  check_sum = 255 - check_sum;
  Serial.write(check_sum);
}